#!/usr/bin/python
# -*- coding:Utf-8 -*- 
# utils for handymenu

import os
import pickle
import gettext

# options for handymenu
menuname = "Bienvenue!"
configfile=os.path.expanduser('~/.accueil.conf')
noclose=os.path.expanduser('~/.handymenu-noclose.conf')
hmdir="/usr/share/accueil"
pixmaps="/usr/share/pixmaps"
configcmd="python {} &".format(os.path.join(hmdir,"handymenu-configuration.py")) 
primtux_icons=os.path.join(pixmaps,hmdir,"icons")
primtuxmenuicon=os.path.join(primtux_icons,"primtuxmenu_icon.png")

onglet_width = 12
maxonglets = 3
iconsize = 85

gettext.bindtextdomain('handymenu', '/usr/share/locale')
gettext.textdomain('handymenu')
_ = gettext.gettext

def set_default_config():
    print("reset configuration")
    with open(configfile, 'wb') as pkl:
        pickle.dump(hm_default_sections, pkl, pickle.HIGHEST_PROTOCOL)

def load_config():
    with open(configfile, 'rb') as pkl:
        try:
            config = pickle.load(pkl)
        except: #ancienne configuration ou config erronée?
            set_default_config()
            config = load_config()
        return(config)

def save_config(conf):
    with open(configfile, 'wb') as pkl:
        pickle.dump(conf, pkl, pickle.HIGHEST_PROTOCOL)

def add_section(config, section):
    config.append(section)
    save_config(config)
    
def move_section(config, section, index):
    """move section of +1 or -1
    index = +1  or -1
    """
    toreload=False
    for s in config:
        if s == section:
            idx = config.index(s)
            if index == -1 : # on recule l'application
                if idx > 0 :
                    config[idx], config[idx-1] = config[idx-1], config[idx]
            elif index == 1 : # on avance l'application
                if idx < len(config) - 1:
                    config[idx], config[idx+1] = config[idx+1], config[idx]
            save_config(config)
            toreload=True
            break
    return(toreload)

def add_app(config, section, app):
    for s in config:
        if s == section:
            s['apps'].append(app)
            save_config(config)

def del_app(config, section, app):
    for s in config:
        if s == section:
            s['apps'].remove(app)
            save_config(config)

def mod_app(config, section, app, new):
    for s in config:
        if s == section:
            for a in s['apps']:
                if a == app:
                    a['name'] = new
                    save_config(config)

def mod_app_icon(config, section, app, newicon):
    for s in config:
        if s == section:
            for a in s['apps']:
                if a == app:
                    a['icon'] = newicon
                    save_config(config)

def move_app(config, section, app, index):
    """move app of +1 or -1
    index = +1  or -1
    """
    for s in config:
        if s == section:
            for a in s['apps']:
                if a == app:
                    idx = s['apps'].index(a)

                    if index == -1 : # on recule l'application
                        if idx > 0 :
                            s['apps'][idx -1], s['apps'][idx] = s['apps'][idx], s['apps'][idx-1]
                    elif index == 1 : # on avance l'application
                        if idx < len(s['apps']) - 1:
                            s['apps'][idx], s['apps'][idx+1] = s['apps'][idx+1], s['apps'][idx]
                    save_config(config)
                    break

#app = {'name' : "Description l'application",\
#        'generic' : "Nom générique de l'application",\
#        'icon' : "icône de l'application",\
#        'cmd' : "commande"}
#applist = [app1, app2, app3, ...]
#section = {'name': 'Recent', 'apps' : applist , id : ''}
#sections = [section1, section2,...]
hm_default_sections = \
[\
    {'name' : _("Présentation"),\
    'id': 9,\
    'apps': [\
            {'name' : _("Présentation"),\
        'generic': _("Présentation du système"),\
        'icon' : "/usr/share/accueil/tux_gandalf_le_gris.png",\
        'cmd' : "xdg-open http://wiki.primtux.fr/doku.php/presentation_du_systeme"\
        },\
        {'name' : _("Logithèque"),\
        'generic': _("Logithèque"),\
        'icon' : "/usr/share/accueil/nono-le-petit-tux.png",\
        'cmd' : "xdg-open http://wiki.primtux.fr/doku.php/logitheque"\
        },\
        {'name' : _("Ressources"),\
        'generic': _("Ressources"),\
        'icon' : "/usr/share/accueil/73-63634.png",\
        'cmd' : "xdg-open http://ressources.primtux.fr"\
        },\
        {'name' : _("Favoris"),\
        'generic': _("Favoris"),\
        'icon' : "/usr/share/accueil/241pbhw.jpg.png",\
        'cmd' : "xdg-open http://primtux.fr/favoris"\
	},\
    ]\
    },\

    {'name' : _("Installation"),\
    'id': 0,\
    'apps': [\
            {'name' : _("Installer"),\
        'generic': _("Installer PrimTux"),\
        'icon' : "/usr/share/accueil/40BrunoCB.png",\
        'cmd' : "xdg-open http://wiki.primtux.fr/doku.php/installer_eiffel"\
         },\
         {'name' : _("Premiers Pas"),\
        'generic': _("Premiers pas après installation"),\
        'icon' : "/usr/share/accueil/tux_tool.png",\
        'cmd' : "xdg-open http://wiki.primtux.fr/doku.php/premiers_pas_eiffel"\
        },\
                 {'name' : _("Clavier"),\
        'generic': _("Choix du clavier"),\
        'icon' : "/usr/share/accueil/tux-pianiste_overlord59-tux.png",\
        'cmd' : "fskbsetting"\
        },\
           {'name' : _("Dépôts"),\
        'generic': _("Mettre à jour les dépôts"),\
        'icon' : "/usr/share/accueil/tux102.png",\
        'cmd' : "/usr/bin/maj.sh"\
        },\
                {'name' : _("Flash"),\
        'generic': _("Installer  Flash"),\
        'icon' : "/usr/share/accueil/Flash_Tux_bigger.png",\
        'cmd' : "/usr/bin/flash.sh"\
        },\
          {'name' : _("Java"),\
        'generic': _("Installer Oracle Java"),\
        'icon' : "/usr/share/accueil/tux warrior.png",\
        'cmd' : "/usr/bin/java.sh"\
        },\
           {'name' : _("Wine"),\
        'generic': _("Installer les dépendances de Wine"),\
        'icon' : "/usr/share/accueil/logo_tuxcopy095048.png",\
        'cmd' : "/usr/bin/primtux-wine.sh"\
        },\
       {'name' : _("Webstrict"),\
        'generic': _("Installer webstrict (configurateur de filtrage)"),\
        'icon' : "/usr/share/accueil/tux-gendarme-mobile.png",\
        'cmd' : "/usr/bin/webstrict.sh"\
        },\
    ]\
    },\

    {'name' : _("Support"),\
    'id': 1,\
    'apps': [\
        {'name' : _("Forum "),\
        'generic': _("Forum / Tutos"),\
        'icon' : "/usr/share/accueil/Tux_Avatar__178_.png",\
        'cmd' : "xdg-open http://forum.primtux.fr"\
        },\
        {'name' : _("Utilisation avancée"),\
        'generic': _("Utilisation avancée"),\
        'icon' : "/usr/share/accueil/cyberscooty-tux-graduate.png",\
        'cmd' : "xdg-open http://wiki.primtux.fr/doku.php/plus_loin"\
        },\
        {'name' : _("Ne plus afficher au démarrage"),\
        'generic': _("Ne plus afficher au démarrage"),\
        'icon' : "/usr/share/accueil/illuzmax-panoratux.png",\
        'cmd' : "/usr/bin/accueil-flux"\
        },\
      ]\
}\
]
