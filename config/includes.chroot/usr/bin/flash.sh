#!/bin/bash

gxmessage " Installez Flash après avoir mis à jour les dépôts." -center -title "Information" -font "Sans bold 10" -default "Cancel" -buttons "_Installer Flash":1,"_Annuler":2>/dev/null 

case $? in
	1)
		roxterm -e sudo apt-get install flashplugin-nonfree;;

	2)
		echo "Exit";;
esac