#!/bin/bash

gxmessage " Mettez les dépôts à jour puis
 installez Oracle java." -center -title "Information" -font "Sans bold 10" -default "Cancel" -buttons "_Installer Oracle Java":1,"_Annuler":2>/dev/null 

case $? in
	1)
		roxterm -e sudo apt-get install oracle-java9-installer;;

	2)
		echo "Exit";;
esac
