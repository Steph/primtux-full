#!/bin/bash

gxmessage " Installateur de dépendances pour les exécutables windows.
1 - Installer les fichiers système nécessaires au fonctionnement de clicmenu.
2 - Installer les fichiers système nécessaires au fonctionnement de Raconte-moi.
3 - Restaurer les associations de fichiers que s'est approprié wine." -center -title "Information" -font "Sans bold 10" -default "Cancel" -buttons "_1 - Clicmenu":1,"_2 - Raconte-moi":2,"_3 - Associations de fichiers":3,"_Annuler":4>/dev/null 

case $? in
	1)
		cd "/usr/share/primtux/" && wine-development ClicMenu-systeme-setup.exe;;
	2) 
		cd "/usr/share/primtux/" && wine-development opencodecs_0.84.17359.exe;;
	3) 
		cp -f /usr/share/applications/mimeinfo.cache ~/.local/share/applications/mimeinfo.cache && roxterm -e "sudo cp -f /usr/share/applications/mimeinfo.cache /root/.local/share/applications/mimeinfo.cache";;

	4)
		echo "Exit";;
esac
