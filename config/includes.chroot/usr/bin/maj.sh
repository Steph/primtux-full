#!/bin/bash

gxmessage "Mise à jour des dépôts." -center -title "Information" -font "Sans bold 10" -default "Cancel" -buttons "_Mettre à jour les dépôts":1,"_Annuler":2>/dev/null 

case $? in
	1)
		roxterm -e sudo apt-get update;;

	2)
		echo "Exit";;
esac