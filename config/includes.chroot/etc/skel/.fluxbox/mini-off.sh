#!/bin/bash

gxmessage "- ÊTES-VOUS SÛR?" -center -title "Applications au démarrage" -font "Sans bold 10" -default "Cancel" -buttons "_Oui":1,"_Annuler":2>/dev/null 

case $? in
	1)
		sed -i '33s/^/# /' startup;;
	
	2)
		echo "Exit";;
esac
